---
title: "Teaching"
layout: archive
sitemap: true
permalink: /teaching/
author_profile: true
---

## Leibniz Universität Hannover

### Instructor

- Geometric modeling and Isogeometric analysis (2 lectures - Summer 2020)
- Stabilized finite element methods for computational fluid mechanics. (4 lectures - Spring 2020)
- Test driven development in Julia (8 lectures - Spring 2020)

## The University of Texas at Austin

### Teaching Assistant

- Stabilized and variational multiscale methods in CFD (Spring 2015 and 2017)
- Non-linear static and dynamic finite element analysis (Spring 2016 and 2018)