---
title: "Publications"
layout: archive
sitemap: true
permalink: /publications/
author_profile: true
---

My list of publications is summarized below. For the most part these should be indexed in my [Google Scholar profile](https://scholar.google.com/citations?hl=en&user=-AXkGecAAAAJ) and also be available in [ResearchGate](https://www.researchgate.net/profile/Rene_Hiemstra).


## Peer reviewed journal articles

|Nguyen, T.H., __Hiemstra, R.R.__, Stoter, S.K.F., Schillinger, D. __(2022)__``A variational approach based on perturbed eigenvalue analysis for improving spectral properties of isogeometric multipatch discretizations.'' _Computer Methods in Applied Mechanics and Engineering_, 392, p. 114671.|
|Nguyen, T.H., __Hiemstra, R.R.__, Schillinger, D., __(2022)__. ``Leveraging spectral analysis to elucidate membrane locking and unlocking in isogeometric finite element formulations of the curved Euler–Bernoulli beam.'' _Computer Methods in Applied Mechanics and Engineering_, 388, p.114240. [[link](/assets/papers/nguyen_2022_LeveragingSpectralAnalysisToElucidateMembraneLocking.pdf)]|
|__Hiemstra, R.R.__, Hughes, T.J.R, Reali, A., Schillinger, D. __(2021)__``Removal of spurious outlier frequencies and modes from isogeometric discretizations of second-and fourth-order problems in one, two, and three dimensions.'' _Computer Methods in Applied Mechanics and Engineering_, 387, p. 114115.|
|Mika, M.Ł., Hughes, T.J.R., Schillinger, D., Wriggers, P. and __Hiemstra, R.R.__, __(2021)__. ``A matrix-free isogeometric Galerkin method for Karhunen–Loève approximation of random fields using tensor product splines, tensor contraction and interpolation based quadrature.'' _Computer Methods in Applied Mechanics and Engineering_, 379, p.113730. [[link](/assets/papers/mika_2021_MatrixFreeIsogeometricGalerkinMethodForKLEpproximationOfRandomFields.pdf)]|
|__Hiemstra, R.R.__, Shepherd, K.M., Johnson, M.J., Quan, L. and Hughes, T.J. __(2020)__. Towards untrimmed NURBS: CAD embedded reparameterization of trimmed B-rep geometry using frame-field guided global parameterization. _Computer Methods in Applied Mechanics and Engineering_, 369, p.113227. [[doi](https://doi.org/10.1016/j.cma.2020.113227)] [[link](/assets/papers/hiemstra_2020_TowardsUntrimmedNURBS.pdf)]|
|__Hiemstra, R.R.__, Hughes, T.J.R., Manni, C., Speleers, H. and Toshniwal, D. __(2020)__. A Tchebycheffian Extension of Multidegree B-Splines: Algorithmic Computation and Properties. _SIAM Journal on Numerical Analysis_, 58(2), pp.1138-1163. [[doi](https://doi.org/10.1137/19M1263583)]  [[link](/assets/papers/hiemstra_2020_TchebycheffianExtensionOfMultiDegreeBsplines.pdf)]|
|Toshniwal, D., Speleers, H., __Hiemstra, R.R.__, Manni, C. and Hughes, T.J. __(2020)__. Multi-degree B-splines: Algorithmic computation and properties. _Computer Aided Geometric Design_, 76, p.101792. [[doi](https://doi.org/10.1016/j.cagd.2019.101792)]|
|__Hiemstra, R.R.__, Sangalli, G., Tani, M., Calabro, F., and Hughes, T.J.R. __(2019)__. Fast Formation and Assembly of Finite Element Matrices with Application to Isogeometric Linear Elasticity. _Computer Methods in Applied Mechanics and Engineering_, Volume 355, Pages 234-260. [[doi](https://doi.org/10.1016/j.cma.2019.06.020)]|
|Evans, J.A., __Hiemstra, R.R.__, Hughes, T.J.R., and Reali, A. __(2018)__. Explicit higher-order accurate isogeometric collocation methods for structural dynamics. _Computer Methods in Applied Mechanics and Engineering_, 338: 208-240. [[doi](https://doi.org/10.1016/j.cma.2018.04.008)]|
|Marussig, B., __Hiemstra, R.R.__, and Hughes, T.J.R. __(2018)__. Improved conditioning of isogeometric analysis matrices for trimmed geometries. _Computer Methods in Applied Mechanics and Engineering_, 334: 79-110. [[doi](https://doi.org/10.1016/j.cma.2018.01.052)]|
|Toshniwal, D., Speleers, H., __Hiemstra, R.R.__, and Hughes, T.J.R. __(2017)__. Multi-degree smooth polar splines: A framework for geometric modeling and isogeometric analysis. _Computer Methods in Applied Mechanics and Engineering_, 316: 1005-1061. [[doi](https://doi.org/10.1016/j.cma.2016.11.009)]|
|__Hiemstra, R.R.__, Calabro, F., Schillinger, D., and Hughes, T.J.R. __(2017)__. Optimal and reduced quadrature rules for tensor product and hierarchically refined splines in isogeometric analysis. _Computer Methods in Applied Mechanics and Engineering_, 316: 966-1004. [[doi](https://doi.org/10.1016/j.cma.2016.10.049)]|
|Schillinger, D., Evans, J. A., Frischmann, F., __Hiemstra, R.R.__, Hsu, M.C., and Hughes, T.J.R. __(2015)__.  A collocated C0 finite element method: Reduced quadrature perspective, cost comparison with standard finite elements, and explicit structural dynamics. _International Journal for Numerical Methods in Engineering_, 102: 576-631. [[doi](https://doi.org/10.1002/nme.4783)]|
|__Hiemstra, R.R.__, Toshniwal, D., Huijsmans, R.H.M., and Gerritsma, M.I. __(2014)__. High order geometric methods with exact conservation properties. _Journal of Computational Physics_, 257: 1444-1471. [[doi](https://doi.org/10.1016/j.jcp.2013.09.027)]|
|Palha, A., Rebelo, P.P., __Hiemstra, R.R.__, Kreeft, J., and Gerritsma, M.I. __(2014)__ Physics-compatible discretization techniques on single and dual grids, with application to the Poisson equation of volume forms. _Journal of Computational Physics_, 257: 1394-1422. [[doi](https://doi.org/10.1016/j.jcp.2013.08.005)]|

## Peer reviewed book chapters

|Mika, M.Ł., __Hiemstra, R.R.__, Schillinger D., Hughes, T.J.R., __(2022)__. ``A Comparison of Matrix-Free Isogeometric Galerkin and Collocation Methods for Karhunen–Loève Expansion. '' _Current Trends and Open Problems in Computational Mechanics_. Springer, Cham, 329-341.|
|__Hiemstra, R. R.__, and Gerritsma, M.I. __(2014)__. High order methods with exact conservation properties. _Spectral and High Order Methods for Partial Differential Equations-ICOSAHOM 2012_ Springer, Cham, 285-295|
|Gerritsma, M. I., __Hiemstra, R. R.__, Kreeft, J., Palha, A., Rebelo, P., and Toshniwal, D. __(2014)__. The geometric basis of numerical methods. _Spectral and High Order Methods for Partial Differential Equations-ICOSAHOM 2012_. Springer, Cham, 17-35.|

<br>

# Presentations

I frequently present my work at international conferences. Below is a list of invited presentations at research institutions and talks at international conferences.

## Invited Seminar Presentations at Research Institutions

|Dagstuhl Workshop on Geometric Modeling: Interoperability and New Challenges. _Schloss Dagstuhl, Germany_ (2021)|
|INdAM Workshop Geometric Challenges in Isogeometric Analysis. _Rome, Italy_ (2020)|
|Isogeometric Splines: Theory and Applications (19w5196). _Banff, Canada_ (2019)|
|Institut für Baumechanik und Numerische Mechanik. _Leibniz University Hannover, Hannover, Germany_ (2019)|
|Department of Civil Engineering and Architecture (DICaR). _University of Pavia, Pavia, Italy_     (2015)|

## Talks at international conferences

|Virtual Isogeometric Analysis 2020 (VIGA2020). _Virtual_ (2020)|
|International Conference on Isogeometric Analysis (IGA2018). _Austin, TX, USA_ (2018)|
|International Conference on Isogeometric Analysis (IGA2017). _Pavia, Italy_ (2017)|
|IACM 19th International Conference on Finite Elements in Flow Problems (FEF-2017). _Rome, Italy_ (2017)|
|USACM Conference on Isogeometric Analysis and Meshfree Methods (IGA2016). _La Jolla, California, USA_ (2016)|
|European Congress on Computational Methods in Applied Sciences and Engineering (ECCOMAS2016). _Crete, Greece_ (2016)|
|15th International Conference on Approximation Theory. _San Antonio, Texas, USA_ (2016)|
|International Conference on Isogeometric Analysis (IGA2015). _Trondheim, Norway_ (2015)|
|Finite Element Rodeo 2014. _Austin, Texas, USA_ (2014)|
|International Conference on Isogeometric Analysis (IGA2014). _Austin, Texas, USA_ (2014)|
|11th. World Congress on Computational Mechanics (WCCM XI). _Barcelona, Spain_ (2014)|
|12th US National congress on Computational Mechanics (USNCCM12). _Raleigh, North Carolina, USA_ (2013)|
|Advances in Computational Mechanics (ACM 2013). _San Diego, California, USA_ (2013)|
|6th European Congress on Computational Methods in Applied Science and Engineering (ECCOMAS2012). _Vienna, Austria_ (2012)|
|International Conference On Spectral And High Order Methods (ICOSAHOM2012). _Gammarth, Tunesia_ (2012)|
|2011 Woudschoten Conference. _Woudschoten, Netherlands_ (2011)|