---
title: ""
layout: archive
sitemap: true
permalink: /
author_profile: true
---


<!-- <img src="/assets/images/rene.png" width="240px" alt="Rene Hiemstra" align="right" /> -->

# About

I am a post-doctoral researcher in the research group of [Prof. Schillinger](https://scholar.google.com/citations?user=3nwlXXsAAAAJ&hl=de) at the Institute of Mechanics at the Technical University of Darmstadt. I have an interdiciplinary background with an M.Sc. and Ph.D. in Computational Science, Engineering and Mathematics from the Oden Institute for Computational Engineering and Sciences at the University of Texas at Austin. I spent six years in Austin, from 2013 - 2019, first obtaining an M.Sc. and later my Ph.D. under supervision of [Professor Thomas J.R. Hughes](https://scholar.google.com/citations?hl=de&user=6tPdlYAAAAAJ). I also hold a B.Sc. and M.Sc in Maritime Technology from Delft University of Technology.

My research interests and focus are on the development and implementation of new methods that overcome limitations of today’s computational techniques in the areas of numerical simulation, machine learning, and geometry processing. A central theme in my research is to improve how these areas interface, foremost in the context of isogeometric analysis.

I have developed finite element exterior calculus techniques for incompressible flow, advanced quadrature, formation, assembly and solution procedures for finite element methods, investigated improved mathematical representations for splines, and worked on algorithmic improvements for PDE-based surface parameterization and mesh generation. Currently, I am continuing and extending these lines of research, co-supervise four PhD students, and am leading our research group’s software development initiative.